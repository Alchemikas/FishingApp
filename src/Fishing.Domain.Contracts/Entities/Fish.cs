﻿using System;

namespace Fishing.Domain.Contracts.Entities
{
    public class Fish
    {
        public string Title { get; set; }
        public double Weight { get; set; }
        public DateTime Time { get; set; }
        public double Lenght { get; set; }
        public string Comment { get; set; }
    }
}
