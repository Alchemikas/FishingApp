﻿using System;
using System.Collections.Generic;
using Fishing.Domain.Contracts.Entities;

namespace Fishing.Domain.Contracts.Events
{
    public class FishsCaughted : EventBase
    {
        public FishsCaughted(Guid correlationId) : base(correlationId)
        {
        }
        public Guid FishingLogId { get; set; }
        public IList<Fish> Fishes { get; set; }
    }
}
