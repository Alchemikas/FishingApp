﻿using System;

namespace Fishing.Domain.Contracts.Events
{
    public class FishingLogUpdated : EventBase
    {
        public FishingLogUpdated(Guid correlationId) : base(correlationId)
        {
        }

        public Guid FishingLogId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Highlight { get; set; }
    }
}
