﻿using System;

namespace Fishing.Domain.Contracts.Events
{
    public interface IEvent : IMessage
    {
        Guid CorrelationId { get; }
        DateTime EventDate { get; }
        EventHeader Header { get; set; }
    }

    public class EventHeader
    {
        public long TimeStamp { get; set; }
        public Guid Id { get; set; }
        public int Version { get; set; }

        public EventHeader(int version)
        {
            TimeStamp = DateTime.UtcNow.Ticks;
            Id = Guid.NewGuid();
            Version = version;
        }
    }
}
