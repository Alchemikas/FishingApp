﻿using System;
using Fishing.Domain.Contracts.Entities;

namespace Fishing.Domain.Contracts.Events
{
    public class FishingLogCreated : EventBase
    {
        public FishingLogCreated(Guid correlationId) : base(correlationId)
        {
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Location Location { get; set; }
        public string Highlight { get; set; }
    }
}
