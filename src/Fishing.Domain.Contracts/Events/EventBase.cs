﻿using System;

namespace Fishing.Domain.Contracts.Events
{
    public abstract class EventBase : IEvent
    {
        protected EventBase(Guid correlationId)
        {
            CorrelationId = correlationId;
            EventDate = DateTime.UtcNow;
        }

        public Guid CorrelationId { get; }
        public DateTime EventDate { get; }
        public EventHeader Header { get; set; }
    }
}
