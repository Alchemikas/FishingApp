﻿using System;

namespace Fishing.Domain.Contracts
{
    public class MessageResponse
    {
        public Status Status { get; set; }
        public Guid Uuid { get; set; }
    }

    public enum Status
    {
        Fail = 0,
        Success = 1
    }
}
