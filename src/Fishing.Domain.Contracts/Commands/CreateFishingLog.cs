﻿using System;
using Fishing.Domain.Contracts.Entities;

namespace Fishing.Domain.Contracts.Commands
{
    public class CreateFishingLog : CommandBase
    {
        public CreateFishingLog(Guid correlationId) : base(correlationId)
        {
        }

        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Location Location { get; set; }
        public string Highlight { get; set; }
    }
}
