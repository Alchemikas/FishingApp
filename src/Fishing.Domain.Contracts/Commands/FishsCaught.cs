﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Entities;

namespace Fishing.Domain.Contracts.Commands
{
    public class FishsCaught : CommandBase
    {
        public FishsCaught(Guid correlationId) : base(correlationId)
        {
        }
        public Guid FishingLogId { get; set; }
        public IList<Fish> Fishes { get; set; }


    }
}
