﻿using System;

namespace Fishing.Domain.Contracts.Commands
{
    public class UpdateFishingLog : CommandBase
    {
        public UpdateFishingLog(Guid correlationId) : base(correlationId)
        {
        }

        public UpdateFishingLog() : base(Guid.NewGuid())
         {
            
        }

        public Guid FishingLogId { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Highlight { get; set; }
    }
}
