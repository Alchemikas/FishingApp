﻿using System;

namespace Fishing.Domain.Contracts.Commands
{
    public abstract class CommandBase : ICommand
    {
        protected CommandBase(Guid correlationId)
        {
            CorrelationId = correlationId;
            CommandDate =  DateTime.UtcNow;
        }

        public Guid CorrelationId { get; }
        public DateTime CommandDate { get; }
        public Guid UserId { get; set; }
    }
}
