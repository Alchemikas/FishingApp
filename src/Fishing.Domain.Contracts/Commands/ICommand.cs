﻿using System;

namespace Fishing.Domain.Contracts.Commands
{
    public interface ICommand : IMessage
    {
        Guid CorrelationId { get; }
        DateTime CommandDate { get; }
    }
}
