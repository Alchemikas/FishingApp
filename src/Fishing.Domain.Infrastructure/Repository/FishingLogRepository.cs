﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EventStore;
using Fishing.Domain.Contracts.Events;
using Fishing.Domain.Core.Aggregates;
using Fishing.Domain.Core.Repository;

namespace Fishing.Domain.Infrastructure.Repository
{
    public class FishingLogRepository : BaseRepository, IRepository<FishingLogAggregte>
    {
        private readonly IEventStore _eventStore;

        public FishingLogRepository(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public async Task SaveAsync(FishingLogAggregte aggregate)
        {
            var events = aggregate.GetUncommittedEvents();
            var streamName = new Identity(aggregate.State.Id, aggregate.GetType().Name).ToString();
            var expectedVersion = aggregate.State.Version;
            await _eventStore.UpdateStreamAsync(streamName, aggregate.State.Id, expectedVersion, events).ConfigureAwait(false);
        }

        public async Task<List<IEvent>> GetById(Guid id)
        {
            var identity = new Identity(id, typeof(FishingLogAggregte).Name);
            EventStream events = await _eventStore.LoadEventStreamAsync(identity.ToString()).ConfigureAwait(false);
            return events.Events;
        }
    }
}
