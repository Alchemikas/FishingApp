﻿using System;

namespace Fishing.Domain.Infrastructure
{
    public class Identity
    {
        public Guid Id { get; }
        public string AggregateType { get; }

        public Identity(Guid id, string aggregateType)
        {
            Id = id;
            AggregateType = aggregateType;
        }

        public override string ToString()
        {
            return $"{AggregateType}-{Id}";
        }
    }
}
