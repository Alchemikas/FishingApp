﻿using System;
using EasyNetQ;
using EventStore;
using Fishing.Domain.ApplicationService;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Core.Aggregates;
using Fishing.Domain.Core.Repository;
using Fishing.Domain.Host.Dispacher;
using Fishing.Domain.Host.Handlers;
using Fishing.Domain.Host.MessageBusConfiguration;
using Fishing.Domain.Infrastructure.Repository;
using Messaging;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client.Impl;
using EventStore = EventStore.EventStore;

namespace Fishing.Domain.Host
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }


        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://localhost:5000")
                .CaptureStartupErrors(true)
                .UseStartup<Startup>()
                .UseDefaultServiceProvider(options =>
                    options.ValidateScopes = false)
                .Build();
    }

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
//            DependencyResolver.RegisterServices(services);
            var a_messagingConfiguration = new MessageBusConfiguration.MessageBusConfiguration(
                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=60",
                "FishingJournal", "domain");
            services.AddSingleton<IEventStore>(x => new global::EventStore.EventStore("ConnectTo=tcp://admin:changeit@localhost:1113; HeartBeatTimeout=500"));
            ;

            services.AddScoped(typeof(IRepository<FishingLogAggregte>), typeof(FishingLogRepository));


            services.AddSingleton(a_messagingConfiguration.Bus);
            services.AddSingleton<ICommandSender, CommandSender>();

            //            serviceCollection.AddTransient<IEventHandler<Command>, FishingLogHandler>();

            services.AddScoped(typeof(IEventHandler<UpdateFishingLog>), typeof(FishingLogHandler));
            services.AddScoped(typeof(IEventHandler<CreateFishingLog>), typeof(FishingLogHandler));

//            var serviceProvider = services.BuildServiceProvider();
            services.AddSingleton<IDomainEventPublisher, DomainEventPublisher>(x =>
            {
                var service = x.GetService<IBus>();
                return new DomainEventPublisher(service);
            });

            services.AddScoped(typeof(IApplicationService), typeof(FishingLogApplicationService));
            services.AddScoped(typeof(IApplicationServiceResponse), typeof(FishingLogApplicationService));

//            serviceProvider = services.BuildServiceProvider();
            services.AddScoped<IMessageDispacher, MessageDispacher>(x =>
            {
                var a = x.GetServices<IApplicationService>();
                var b = x.GetServices<IApplicationServiceResponse>();
                return new MessageDispacher(a, b);
            });

            var serviceProvider = services.BuildServiceProvider();
            a_messagingConfiguration.Subscribe(serviceProvider);

            
//            messageBusConfiguration.Subscribe(serviceProvider);
        }

        public void Configure(IApplicationBuilder app, IApplicationLifetime appLifetime, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            appLifetime.ApplicationStarted.Register(OnStarted);
            appLifetime.ApplicationStopping.Register(OnStopping);
            appLifetime.ApplicationStopped.Register(OnStopped);

            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                appLifetime.StopApplication();
                // Don't terminate the process immediately, wait for the Main thread to exit gracefully.
                eventArgs.Cancel = true;
            };
        }

        private void OnStarted()
        {
            // Perform post-startup activities here
        }

        private void OnStopping()
        {
            // Perform on-stopping activities here
        }

        private void OnStopped()
        {
            // Perform post-stopped activities here
        }
    }
}