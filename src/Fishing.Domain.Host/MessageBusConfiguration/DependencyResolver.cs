﻿using System;
using Fishing.Domain.ApplicationService;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Host.Dispacher;
using Fishing.Domain.Host.Handlers;
using Messaging;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client.Impl;

namespace Fishing.Domain.Host.MessageBusConfiguration
{
    public static class DependencyResolver
    {
        static MessageBusConfiguration _messagingConfiguration; 
            
        public static void RegisterServices(IServiceCollection serviceCollection)
        {
            
            _messagingConfiguration = new MessageBusConfiguration(
                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=60",
                "FishingJournal", "domain");
            RegisterMessaging(serviceCollection);
        }

        private static void RegisterMessaging(IServiceCollection serviceCollection)
        {

            var a_messagingConfiguration = new MessageBusConfiguration(
                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=60",
                "FishingJournal", "domain");
            serviceCollection.AddSingleton<IMessageDispacher, MessageDispacher>();
            serviceCollection.AddScoped(typeof(IEventHandler<UpdateFishingLog>), typeof(FishingLogHandler));
            serviceCollection.AddScoped(typeof(IApplicationService), typeof(FishingLogApplicationService));


            serviceCollection.AddSingleton(_messagingConfiguration.Bus);
            serviceCollection.AddSingleton<ICommandSender, CommandSender>();

            //            serviceCollection.AddTransient<IEventHandler<Command>, FishingLogHandler>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            a_messagingConfiguration.Subscribe(serviceProvider);
        }

        public static void RR(IServiceProvider serviceProvider)
        {
            _messagingConfiguration.Subscribe(serviceProvider);
        }
    }
}
