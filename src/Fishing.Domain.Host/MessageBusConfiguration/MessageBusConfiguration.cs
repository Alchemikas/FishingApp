﻿using EasyNetQ;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Host.Handlers;
using Messaging;

namespace Fishing.Domain.Host.MessageBusConfiguration
{
    public class MessageBusConfiguration : MessageBusConfigurationBase
    {
        public MessageBusConfiguration(string connectionString, string subscriptionId, string space) : base(connectionString, subscriptionId, space)
        {
        }

        public MessageBusConfiguration(IBus bus) : base(bus)
        {
        }

        protected override void SubscribeEvents()
        {
                        RegisterHandler<UpdateFishingLog, FishingLogHandler>();
                        RegisterHandler<CreateFishingLog, FishingLogHandler>();
        }
    }
}
