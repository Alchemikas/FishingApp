﻿using System.Threading.Tasks;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Host.Dispacher;
using Messaging;

namespace Fishing.Domain.Host.Handlers
{
    public class FishingLogHandler :
        IEventHandler<CreateFishingLog>,
        IEventHandler<UpdateFishingLog>
    {
        private readonly IMessageDispacher _messageDispacher;

        public FishingLogHandler(IMessageDispacher messageDispacher)
        {
            _messageDispacher = messageDispacher;
        }

        public async Task HandleAsync(CreateFishingLog message)
        {
            await Task.Yield();
            _messageDispacher.Dispatch(message);
        }

        public async Task HandleAsync(UpdateFishingLog message)
        {
            await Task.Yield();
            _messageDispacher.Dispatch(message);
        }
    }
}
