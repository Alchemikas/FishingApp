﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fishing.Domain.ApplicationService;
using Fishing.Domain.Contracts;

namespace Fishing.Domain.Host.Dispacher
{
    public class MessageDispacher : IMessageDispacher
    {
        private readonly IList<IApplicationService> _commandHandlers = new List<IApplicationService>();
        private readonly IList<IApplicationServiceResponse> _commandHandlersResponse = new List<IApplicationServiceResponse>();

        public MessageDispacher(IEnumerable<IApplicationService> handlers, IEnumerable<IApplicationServiceResponse> hh)
        {
            foreach (var handler in handlers)
            {
                _commandHandlers.Add(handler);
            }

            foreach (var handler in hh)
            {
                _commandHandlersResponse.Add(handler);
            }
        }
        public void Dispatch(IMessage cmd)
        {
            foreach (var handler in _commandHandlers)
            {
                handler.Handle(cmd);
            }
        }

        public void Dispatch(IEnumerable<IMessage> commands)
        {
            foreach (var cmd in commands)
            {
                Dispatch(cmd);
            }
        }

        public async Task<MessageResponse> DispatchAndGetResponse(IMessage cmd)
        {
            MessageResponse aaa = null;
            foreach (var handler in _commandHandlersResponse)
            {
                aaa = await handler.Handle(cmd);
            }
            return aaa;
        }
    }
}
