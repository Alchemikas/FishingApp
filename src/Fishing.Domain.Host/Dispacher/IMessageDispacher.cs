﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Fishing.Domain.Contracts;

namespace Fishing.Domain.Host.Dispacher
{
    public interface IMessageDispacher
    {
        void Dispatch(IMessage cmd);
        void Dispatch(IEnumerable<IMessage> commands);
        Task<MessageResponse> DispatchAndGetResponse(IMessage cmd);
    }
}
