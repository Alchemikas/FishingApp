﻿using System.Threading.Tasks;

namespace Fishing.ReadModels.Services
{
    public interface IProjection<TState> where TState : class
    {
        Task<TState> ProjectAsync(ProjectionData projectionSource);
    }
}
