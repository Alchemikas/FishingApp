﻿using Fishing.Domain.Contracts.Events;
using Fishing.ReadModels.Contracts.ReadModels;

namespace Fishing.ReadModels.Services.Projections
{
    public class FishingLogProjection : Projection<FishingLog>
    {
        public FishingLog ApplyEvent(FishingLog state, FishingLogCreated e)
        {
            state.Id = e.Id;
            state.Comment = e.Comment;
            state.EndDate = e.EndDate;
            state.Highlight = e.Highlight;
            state.Highlight = e.Highlight;
            state.Title = e.Title;
            return state;
        }

        public FishingLog ApplyEvent(FishingLog state, FishingLogUpdated e)
        {
            state.Comment = e.Comment;
            state.EndDate = e.EndDate;
            state.Highlight = e.Highlight;
            return state;
        }
    }
}
