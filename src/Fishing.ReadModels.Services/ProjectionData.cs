﻿using System;
using System.Collections.Generic;
using EventStore;
using Fishing.Domain.Contracts.Events;

namespace Fishing.ReadModels.Services
{
    public class ProjectionData
    {
        private readonly EventStream _stream;

        public ProjectionData(EventStream stream)
        {
            _stream = stream ?? throw new ArgumentException(nameof(stream));
        }

        public IReadOnlyList<IEvent> Events()
        {
            return _stream.Events.AsReadOnly();
        }
    }
}
