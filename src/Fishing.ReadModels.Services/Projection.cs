﻿using System;
using System.Threading.Tasks;

namespace Fishing.ReadModels.Services
{
    public abstract class Projection<TState> : IProjection<TState> where TState : class
    {
        public Task<TState> ProjectAsync(ProjectionData projectionSource)
        {
            if (projectionSource == null)
                throw new ArgumentNullException(nameof(projectionSource));

            var state = Activator.CreateInstance<TState>();

            foreach (var @event in projectionSource.Events())
            {
                // if event is eqiuvalent to sate just re-assign it;
                // doing this in Apply event scope does not prevent instace order (weird behaviour...), so avoid doing this
                if (@event is TState)
                {
                    state = (TState)@event;
                    continue;
                }

                Apply(state, @event);
            }

            return Task.FromResult(state);
        }

        private void Apply(TState state, object @event)
        {
            ((dynamic)this).ApplyEvent(state, (dynamic)@event);
        }
    }
}
