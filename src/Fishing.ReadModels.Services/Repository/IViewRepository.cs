﻿using System;
using System.Threading.Tasks;

namespace Fishing.ReadModels.Services.Repository
{
    public interface IViewRepository<TView>
    {
        Task<TView> GetAsync(Guid id);
        Task SaveAsync(TView view);
    }
}
