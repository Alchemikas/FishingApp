﻿using System;
using System.Threading.Tasks;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Services.Repository;

namespace Fishing.ReadModels.Services.Services
{
    public interface IFishingLogApplicationService
    {
        Task<FishingLog> GetFishingLog(Guid id);
    }

    public class FishingLogApplicationService
    {
        private readonly IViewRepository<FishingLog> _repository;

        public FishingLogApplicationService(IViewRepository<FishingLog> repository)
        {
            _repository = repository;
        }

        // could be done some filtering like OData
        public async Task<FishingLog> GetFishingLog(Guid id)
        {
            var fishingLog = await _repository.GetAsync(id);
            return fishingLog;
        }
    }
}
