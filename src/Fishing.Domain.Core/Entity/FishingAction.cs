﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fishing.Domain.Core.Entity
{
    public class FishingAction
    {
        public FishingAction(Time time, string name, string highlight, string comment)
        {
            Time = time;
            Name = name;
            Highlight = highlight;
            Comment = comment;
        }

        private Time Time { get; set; }
        private string Name { get; set; }
        private string Highlight { get; set; }
        private string Comment { get; set; }
    }

    public class Time
    {
        public Time(DateTime start, DateTime stop)
        {
            Start = start;
            Stop = stop;
        }

        private DateTime Start { get; set; }
        private DateTime Stop { get; set; }
        private DateTime Duration { get; set; }
    }
}
