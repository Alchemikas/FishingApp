﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fishing.Domain.Contracts.Events;

namespace Fishing.Domain.Core.Repository
{
    public interface IRepository<TAggregateRoot>
    {
        Task SaveAsync(TAggregateRoot aggregate);
        Task<List<IEvent>> GetById(Guid id);
    }
}
