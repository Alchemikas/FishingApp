﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Entities;
using Fishing.Domain.Contracts.Events;
using Fishing.Domain.Core.Aggregates.Base;
using Fishing.Domain.Core.Entity;

namespace Fishing.Domain.Core.Aggregates
{
    public class FishingLogState : AggregateState
    {
        private FishingAction FishingAction { get; set; }

        public FishingLogState()
        {
            Fishs = new List<Fish>();
        }

        private List<Fish> Fishs { get; set; }

        public void When(FishsCaughted e)
        {
            Fishs.AddRange(e.Fishes);
        }

        public void When(FishingLogCreated e)
        {
            Id = e.Id;
            FishingAction = new FishingAction(new Time(e.StartDate, e.EndDate), e.Title, e.Highlight, e.Comment);
        }

        public void When(FishingLogUpdated e)
        {
            FishingAction = new FishingAction(new Time(e.StartDate, e.EndDate), e.Title, e.Highlight, e.Comment);
        }

    }
}
