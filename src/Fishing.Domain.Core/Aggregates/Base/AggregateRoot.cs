﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Events;

namespace Fishing.Domain.Core.Aggregates.Base
{
    public abstract class AggregateRoot<TState> : IAggregateRoot where TState : AggregateState
    {
        public int Version => State.Version;

        private readonly IList<IEvent> _changes = new List<IEvent>();
        public TState State { get; }
        public IList<IEvent> GetUncommittedEvents()
        {
            return _changes;
        }

        protected AggregateRoot(TState state)
        {
            State = state;
        }

        protected AggregateRoot(TState state, IEnumerable<IEvent> events)
        {
            if (events != null)
            {
                foreach (var @event in events)
                {
                    state.Mutate(@event);
                }
            }
            State = state;
        }

        public void Apply(IEvent e)
        {
            e.Header = new EventHeader(State.Version + 1);
            State.Mutate(e);
            _changes.Add(e);
        }

        public void ClearUncommittedEvents()
        {
            _changes.Clear();
        }

    }
}
