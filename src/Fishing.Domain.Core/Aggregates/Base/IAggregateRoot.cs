﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Events;

namespace Fishing.Domain.Core.Aggregates.Base
{
    public interface IAggregateRoot
    {
        int Version { get; }
        void Apply(IEvent e);
        void ClearUncommittedEvents();
        IList<IEvent> GetUncommittedEvents();
    }
}
