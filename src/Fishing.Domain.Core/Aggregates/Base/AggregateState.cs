﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Events;

namespace Fishing.Domain.Core.Aggregates.Base
{
    public abstract class AggregateState
    {
        public Guid Id { get; set; }

        protected AggregateState()
        {
            Version = -1;
        }

        public int Version { get; private set; }

        protected AggregateState(IEnumerable<IEvent> events)
        {
            foreach (var @event in events)
            {
                Mutate(@event);
            }
        }

        public void Mutate(IEvent e)
        {
            if (Version < e.Header.Version)
            {
                Version = e.Header.Version;
            }

            ((dynamic)this).When((dynamic)e);
        }
    }
}
