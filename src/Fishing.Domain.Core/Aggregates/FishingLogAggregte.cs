﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Contracts.Events;
using Fishing.Domain.Core.Aggregates.Base;

namespace Fishing.Domain.Core.Aggregates
{
    public class FishingLogAggregte : AggregateRoot<FishingLogState>
    {
        public FishingLogAggregte(FishingLogState state) : base(state)
        {
        }

        public FishingLogAggregte(IEnumerable<IEvent> events) : base(new FishingLogState(), events)
        {
        }

        public FishingLogAggregte() : base(new FishingLogState())
        {

        }

        public void FishsCaught(FishsCaught cmd)
        {
            var @event = new FishsCaughted(cmd.CorrelationId)
            {
                Fishes = cmd.Fishes,
                FishingLogId = cmd.FishingLogId
            };
            Apply(@event);
        }

        public void Update(UpdateFishingLog cmd)
        {
            var @event = new FishingLogUpdated(cmd.CorrelationId)
            {
                FishingLogId = cmd.FishingLogId,
                Comment = cmd.Comment,
                Title = cmd.Title,
                StartDate = cmd.StartDate,
                EndDate = cmd.EndDate,
                Highlight = cmd.Highlight,
            };
            Apply(@event);
        }

        public void Create(CreateFishingLog cmd)
        {
            var @event = new FishingLogCreated(cmd.CorrelationId)
            {
                Id = Guid.NewGuid(),
                Comment = cmd.Comment,
                Location = cmd.Location,
                Title = cmd.Title,
                StartDate = cmd.StartDate,
                EndDate = cmd.EndDate,
                Highlight = cmd.Highlight,
            };
            Apply(@event);
        }
    }
}
