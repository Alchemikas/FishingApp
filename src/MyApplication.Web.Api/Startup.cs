﻿using AutoMapper;
using Infrastructure;
using Infrastructure.CorrelationIdMiddleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyApplication.Web.Api.Configuration.IOC;
using MyApplication.Web.Api.Configuration.Mapping;
using MyApplication.Web.Api.Handlers;
using Swashbuckle.AspNetCore.Swagger;

namespace MyApplication.Web.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            DependencyResolver.RegisterServices(services);
            services.AddScoped<IRequestInfo, RequestInfo>();
            services.AddTransient<ICorrelationContextAccessor, CorrelationContextAccessor>();

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperProfileConfiguration());
            });

            var mapper = config.CreateMapper();
            mapper.ConfigurationProvider.AssertConfigurationIsValid();

            
            services.AddSingleton(mapper);
            
            services.BuildServiceProvider();

                        services.AddSwaggerGen(options =>
                        {
                            options.SwaggerDoc("v1",
                                new Info
                                {
                                    Title = "MyApplication.Web.Api",
                                    Version = "v1",
                                    Description = "Some API description",
                                    TermsOfService = "None"
                                }
                            );
                          
                            options.DescribeAllEnumsAsStrings();
                        });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
//                app.UseDeveloperExceptionPage();
                app.UseExceptionHandlingMiddleware();
            }
            else
            {
                app.UseExceptionHandlingMiddleware();
                app.UseExceptionHandler();
            }
            RequestCorrelationOptions a = new RequestCorrelationOptions();
            a.IncludeCorrelationIdInResponse = false;
            a.IncludeRequestIdInResponse = false;
            app.UseIncomingRequestMiddleware(a);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller}/{action}/{id?}"
                );
            });


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

        }
    }
}
