﻿using System;
using AutoMapper;
using Fishing.Domain.Contracts.Commands;
using MyApplication.Contract.Models.DTO.FishingLog;

namespace MyApplication.Web.Api.Configuration.Mapping
{
    public class AutoMapperProfileConfiguration : Profile
    {
        private const string CorrelationId = "correlationId";
        public AutoMapperProfileConfiguration()
            : this("MapperProfile")
        {
        }
        protected AutoMapperProfileConfiguration(string profileName)
            : base(profileName)
        {
            CreateMap<CreateFishingLogDto, CreateFishingLog>().ForMember(x => x.UserId, y => y.Ignore())
                .ForCtorParam(CorrelationId, opt => opt.ResolveUsing(x => Guid.NewGuid()));

        }
    }

    
}
