﻿using EasyNetQ;
using Messaging;

namespace MyApplication.Web.Api.Configuration
{
    public class MessageBusConfiguration : MessageBusConfigurationBase
    {
        public MessageBusConfiguration(string connectionString, string subscriptionId, string space) : base(connectionString, subscriptionId, space)
        {
        }

        public MessageBusConfiguration(IBus bus) : base(bus)
        {
        }

        protected override void SubscribeEvents()
        {
//            RegisterHandler<UpdateFishingLog, FishingLogHandler>();
        }
    }

//    public class FishingLogHandler : IEventHandler<Command>
//    {
//        public async Task HandleAsync(Command message)
//        {
//            var a = message;
//            await Task.Yield();
//        }
//    }
}
