﻿using System;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Host.Handlers;
using Messaging;
using Microsoft.Extensions.DependencyInjection;
using MyApplication.Web.Api.Controllers;

namespace MyApplication.Web.Api.Configuration.IOC
{
    public static class DependencyResolver
    {
            
        public static void RegisterServices(IServiceCollection serviceCollection)
        {
            
//            _messagingConfiguration = new MessageBusConfiguration(
//                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=60",
//                "FishingJournal", "api");
            RegisterMessaging(serviceCollection);
        }

        private static void RegisterMessaging(IServiceCollection serviceCollection)
        {

            var amessagingConfiguration = new MessageBusConfiguration(
                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=3000",
                "FishingJournal", "api");
            serviceCollection.AddSingleton(amessagingConfiguration.Bus);
            serviceCollection.AddSingleton<ICommandSender, CommandSender>();


            serviceCollection.AddScoped<IQueryDispatcher, QueryDispatcher>(x =>
            {
                var services = serviceCollection.BuildServiceProvider();
                return new QueryDispatcher(services);
            });


            //            serviceCollection.AddTransient<IEventHandler<Command>, FishingLogHandler>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            amessagingConfiguration.Subscribe(serviceProvider);
        }
    }
}
