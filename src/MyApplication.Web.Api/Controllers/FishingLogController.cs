﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Fishing.Domain.Contracts.Commands;
using Infrastructure.CorrelationIdMiddleware;
using Messaging;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.AspNetCore.Mvc;
using MyApplication.Contract.Models;
using MyApplication.Contract.Models.DTO.FishingLog;
using MyApplication.Contract.Models.Response;
using Newtonsoft.Json.Serialization;

namespace MyApplication.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class FishingLogController : Controller
    {
        private readonly ICommandSender _commandSender;
        private readonly IQueryDispatcher _queryDispatcher;
        private readonly ICorrelationContextAccessor _requestInfo;
        private readonly IMapper _mapper;

        public FishingLogController(ICommandSender commandSender,
            IQueryDispatcher queryDispatcher,
            ICorrelationContextAccessor requestInfo,
            IMapper mapper)
        {
            _commandSender = commandSender;
            _queryDispatcher = queryDispatcher;
            _requestInfo = requestInfo;
            _mapper = mapper;
        }

        [HttpGet("{uuid}")]
        [Produces(typeof(FishingLogResponse))]
        public async Task<IActionResult> GetFishingLog(Guid uuid)
        {
            var request = new GetFishingLogRequest();
            FishingLogResponse response = await _queryDispatcher.Execute<GetFishingLogRequest, FishingLogResponse>(request).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpGet("")]
        [Produces(typeof(FishingLogsResponse))]
        public async Task<IActionResult> GetAllFishingLogs()
        {
            var request = new GetFishingLogsRequest();
            var response = await _queryDispatcher.Execute<GetFishingLogsRequest, FishingLogsResponse>(request).ConfigureAwait(false);
            return Ok(response);
        }

        [HttpPost]
        [Produces(typeof(FishingLogsResponse))]
        public async Task<IActionResult> CreateFishingLog([FromBody]CreateFishingLogDto request)
        {
            var cmd = _mapper.Map<CreateFishingLog>(request);
            await _commandSender.PublishAsync(cmd).ConfigureAwait(false);
            return await Task.FromResult(Ok());
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(Guid id, [FromBody]JsonPatchDocument<UpdateFishingLog> request)
        {
            var patch = new JsonPatchDocument<UpdateFishingLog>(new List<Operation<UpdateFishingLog>>(), new CamelCasePropertyNamesContractResolver());
            return await Task.FromResult(Ok());
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid uuid, [FromBody]UpdateFishingLog request)
        {
            var cmd = new UpdateFishingLog(request.CorrelationId)
            {
                FishingLogId = uuid,
                Comment = request.Comment,
                EndDate = request.EndDate,
                Highlight = request.Highlight,
                StartDate = request.StartDate,
                Title = request.Title
            };

            await _commandSender.PublishAsync(cmd).ConfigureAwait(false);
            return await Task.FromResult(Ok());
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return await Task.FromResult(Ok());
        }
    }
}
