﻿using System;
using System.Collections.Generic;

namespace MyApplication.Web.Api.Exceptions
{
    public abstract class BaseException : Exception
    {
        private readonly string _message;
        private readonly Dictionary<string, object> _params;

        protected BaseException(string message)
        {
            _message = message;
        }

        protected BaseException(string message, Dictionary<string, object> @params)
        {
            _message = message;
            _params = @params;
        }
    }

    public class ClientErrorException : BaseException
    {
        public ClientErrorException(string message) : base(message)
        {
        }

        public ClientErrorException(string message, Dictionary<string, object> @params) : base(message, @params)
        {
        }
    }

    public class ServerErrorException : BaseException
    {
        public ServerErrorException(string message) : base(message)
        {
        }

        public ServerErrorException(string message, Dictionary<string, object> @params) : base(message, @params)
        {
        }
    }
}
