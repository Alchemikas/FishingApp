﻿using System;
using System.Threading.Tasks;
using EventStore;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Infrastructure.Redis;
using Fishing.ReadModels.Services.Repository;

namespace Fishing.ReadModels.Infrastructure.Repositories
{
    public class FishingLogProjectionRepository : IViewRepository<FishingLog>
    {
        private readonly ICacheProvider _cacheProvider;
        private readonly IEventStore _eventStore;

        public FishingLogProjectionRepository(ICacheProvider cacheProvider, IEventStore eventStore)
        {
            _cacheProvider = cacheProvider;
            _eventStore = eventStore;
        }

        public Task<FishingLog> GetAsync(Guid id)
        {
            return _cacheProvider.Get<FishingLog>(id.ToString());
        }

        public async Task SaveAsync(FishingLog view)
        {
            await _eventStore.UpdateStreamAsync(nameof(FishingLog), view.Id, 1, view);
//            await _cacheProvider.UpdateIfExists(view.Id.ToString(), view);
        }
    }
}
