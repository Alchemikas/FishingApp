﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Fishing.ReadModels.Infrastructure.Redis
{
    public class RedisCacheProvider : ICacheProvider
    {
        private readonly IDatabase _databaseCache;

        public RedisCacheProvider()
        {
            _databaseCache = ReddisConnectionWrapper.RedisCache;
        }

        public async Task Set<T>(string key, T value)
        {
            await _databaseCache.StringSetAsync(key, JsonConvert.SerializeObject(value));
        }

        public async Task UpdateIfExists<T>(string key, T value)
        {
            if (await IsInCache(key))
            {
                await Remove(key);
            }
            await _databaseCache.StringSetAsync(key, JsonConvert.SerializeObject(value));
        }

        public async Task Set<T>(string key, T value, TimeSpan timeToLive)
        {
            await _databaseCache.StringSetAsync(key, JsonConvert.SerializeObject(value), timeToLive);
        }

        public async Task<T> Get<T>(string key)
        {
            var result = await _databaseCache.StringGetAsync(key);
            return JsonConvert.DeserializeObject<T>(result);
        }

        public async Task<bool> Remove(string key)
        {
            return await _databaseCache.KeyDeleteAsync(key);
        }

        public async Task<bool> IsInCache(string key)
        {
            return await _databaseCache.KeyExistsAsync(key);
        }

        static byte[] Serialize(object o)
        {
            byte[] objectDataAsStream = null;

            if (o != null)
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    binaryFormatter.Serialize(memoryStream, o);
                    objectDataAsStream = memoryStream.ToArray();
                }
            }

            return objectDataAsStream;
        }

        static T Deserialize<T>(byte[] stream)
        {
            T result = default(T);

            if (stream != null)
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                using (MemoryStream memoryStream = new MemoryStream(stream))
                {
                    result = (T)binaryFormatter.Deserialize(memoryStream);
                }
            }

            return result;
        }
    }
}
