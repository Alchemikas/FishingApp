﻿using System;
using StackExchange.Redis;

namespace Fishing.ReadModels.Infrastructure.Redis
{
    public static class ReddisConnectionWrapper
    {
        private static readonly Lazy<ConnectionMultiplexer> LazyConnection;

        static ReddisConnectionWrapper()
        {
            var configurationOptions = new ConfigurationOptions
            {
                EndPoints = { "localhost" }
            };

            LazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(configurationOptions));
        }

        public static ConnectionMultiplexer Connection => LazyConnection.Value;

        public static IDatabase RedisCache => Connection.GetDatabase();
    }
}
