﻿using System;
using System.Threading.Tasks;

namespace Fishing.ReadModels.Infrastructure.Redis
{
    public interface ICacheProvider
    {
        Task Set<T>(string key, T value);

        Task Set<T>(string key, T value, TimeSpan timeToLive);
        Task UpdateIfExists<T>(string key, T value);

        Task<T> Get<T>(string key);

        Task<bool> Remove(string key);

        Task<bool> IsInCache(string key);
    }
}
