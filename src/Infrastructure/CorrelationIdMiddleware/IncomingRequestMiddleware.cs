﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Infrastructure.CorrelationIdMiddleware
{
    public class IncomingRequestMiddleware
    {
        private readonly RequestDelegate _next;
        public readonly RequestInfo RequestInfo = new RequestInfo();
        private readonly RequestCorrelationOptions _options;

        public IncomingRequestMiddleware(RequestDelegate next, IOptions<RequestCorrelationOptions> options)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            _next = next;
        }

        public Task Invoke(HttpContext context)
        {
            var haveCorrelationId = context.Request.Headers.ContainsKey(RequestCorrelationOptions.CorrelationIdHeader);
            var haveRequestId = context.Request.Headers.ContainsKey(RequestCorrelationOptions.RequestIdHeader);

            // first Api request
            if (!haveCorrelationId)
            {
                var uid = Guid.NewGuid();
                RequestInfo.CorrelationUid = uid;
                RequestInfo.RequestUid = uid;
                RequestInfo.TrigeredByUid = uid;

                HandleResponseCorrelationIds(RequestInfo, context.Response, _options);
                var aa = new CorrelationContextAccessor();
                aa.CorrelationContext = RequestInfo;
                return this._next(context);
            }


            RequestInfo.RequestUid = Guid.NewGuid();
            if (Guid.TryParse(context.Request.Headers[RequestCorrelationOptions.CorrelationIdHeader].First(), out var correlationUid))
            {
                RequestInfo.CorrelationUid = correlationUid;
            }
            else
            {
                // log error 
            }

            if (haveRequestId)
            {
                if (Guid.TryParse(context.Request.Headers[RequestCorrelationOptions.RequestIdHeader].First(), out var requestUid))
                {
                    RequestInfo.TrigeredByUid = requestUid;
                }
                else
                {
                    // log error 
                }
            }

            HandleResponseCorrelationIds(RequestInfo, context.Response, _options);
            var aaa = new CorrelationContextAccessor();
            aaa.CorrelationContext = RequestInfo;
            return this._next(context);
        }

        private void HandleResponseCorrelationIds(IRequestInfo requestInfo, HttpResponse response, RequestCorrelationOptions requestOptions)
        {
            if (requestOptions.IncludeCorrelationIdInResponse)
            {
                response.Headers.Add(RequestCorrelationOptions.CorrelationIdHeader, requestInfo.CorrelationUid.ToString());
            }

            if (requestOptions.IncludeRequestIdInResponse)
            {
                response.Headers.Add(RequestCorrelationOptions.RequestIdHeader, requestInfo.RequestUid.ToString());
            }

            if (requestOptions.IncludeTriggeredByIdInResponse)
            {
                response.Headers.Add(RequestCorrelationOptions.TriggeredByIdHeader, requestInfo.TrigeredByUid.ToString());
            }
        }

}

    public static class IncomingRequestMiddlewareExtensions
    {
        public static IApplicationBuilder UseIncomingRequestMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<IncomingRequestMiddleware>();
        }

        public static IApplicationBuilder UseIncomingRequestMiddleware(
            this IApplicationBuilder builder, RequestCorrelationOptions requestCorrelationOptions)
        {
            return builder.UseMiddleware<IncomingRequestMiddleware>(Options.Create(requestCorrelationOptions));
        }
    }

    public class CorrelationContextAccessor : ICorrelationContextAccessor
    {
        private static AsyncLocal<RequestInfo> _correlationContext = new AsyncLocal<RequestInfo>();

        public RequestInfo CorrelationContext
        {
            get => _correlationContext.Value;
            set => _correlationContext.Value = value;
        }
    }

    public interface ICorrelationContextAccessor
    {
        /// <summary>
        /// The <see cref="CorrelationContext"/> for the current request.
        /// </summary>
        RequestInfo CorrelationContext { get; set; }
    }
}
