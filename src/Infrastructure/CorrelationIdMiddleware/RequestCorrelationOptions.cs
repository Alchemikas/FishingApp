﻿
namespace Infrastructure.CorrelationIdMiddleware
{
    public class RequestCorrelationOptions
    {
        public const string CorrelationIdHeader = "X-Correlation-Id";
        public const string RequestIdHeader = "X-Request-Id";
        public const string TriggeredByIdHeader = "X-Request-Triggered-Id";

        public bool IncludeCorrelationIdInResponse { get; set; } = true;
        public bool IncludeRequestIdInResponse { get; set; } = true;
        public bool IncludeTriggeredByIdInResponse { get; set; } = true;
    }
}
