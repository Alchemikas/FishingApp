﻿using System;

namespace Infrastructure.CorrelationIdMiddleware
{
    public interface IRequestInfo
    {
        Guid CorrelationUid { get; set; }
        Guid RequestUid { get; set; }
        Guid TrigeredByUid { get; set; }
    }
}

