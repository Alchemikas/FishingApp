﻿using System;

namespace Infrastructure.CorrelationIdMiddleware
{
    public class RequestInfo : IRequestInfo
    {
        private Guid _CorrelationUid;

        public Guid CorrelationUid
        {
            get => _CorrelationUid;
            set => _CorrelationUid = value;
        }
        public Guid RequestUid { get; set; }
        public Guid TrigeredByUid { get; set; }
    }
}
