﻿using System;

namespace MyApplication.Contract.Models
{
    public class GetFishingLogRequest : AuditableRequest
    {
        public GetFishingLogRequest() : base(Guid.NewGuid())
        {
        }

        public Guid Id { get; set; }
    }
}
