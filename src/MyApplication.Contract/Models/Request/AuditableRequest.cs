﻿using System;

namespace MyApplication.Contract.Models
{
    public abstract class AuditableRequest : BaseRequest
    {
        protected AuditableRequest(Guid correlationId) : base(correlationId)
        {
        }

        public Guid UserId { get; set; }
    }
}
