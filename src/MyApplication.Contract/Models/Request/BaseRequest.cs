﻿using System;

namespace MyApplication.Contract.Models
{
    public abstract class BaseRequest
    {
        protected BaseRequest(Guid correlationId)
        {
            CorrelationId = correlationId;
        }

        public Guid CorrelationId { get; }
    }
}
