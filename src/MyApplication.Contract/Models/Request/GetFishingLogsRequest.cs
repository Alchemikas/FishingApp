﻿using System;

namespace MyApplication.Contract.Models
{
    public class GetFishingLogsRequest : AuditableRequest
    {
        public GetFishingLogsRequest() : base(Guid.NewGuid())
        {
        }
    }
}
