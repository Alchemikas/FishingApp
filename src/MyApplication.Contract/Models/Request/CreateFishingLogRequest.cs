﻿using System;

namespace MyApplication.Contract.Models
{
    public class CreateFishingLogRequest : AuditableRequest
    {
        public CreateFishingLogRequest() : base(Guid.NewGuid())
        {
        }
    }
}
