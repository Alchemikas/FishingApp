﻿using System;

namespace MyApplication.Contract.Models.DTO.FishingLog
{
    public class UpdateFishingLogDto
    {
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public LocationDto Location { get; set; }
        public string Highlight { get; set; }
    }
}
