﻿using System;

namespace MyApplication.Contract.Models.DTO.FishingLog
{
    public class CreateFishingLogDto
    {
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public LocationDto Location { get; set; }
        public string Highlight { get; set; }
    }

    public class LocationDto
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string City { get; set; }
    }
}
