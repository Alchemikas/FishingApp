﻿using System;

namespace Fishing.ReadModels.Contracts.ReadModels
{

    public abstract class ViewEntity
    {
        public Guid Id { get; set; }
    }

    public class FishingLog : ViewEntity
    {
        public string Title { get; set; }
        public string Comment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Highlight { get; set; }
    }
}
