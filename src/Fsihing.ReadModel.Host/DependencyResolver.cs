﻿using EventStore;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Contracts.Events;
using Fishing.Domain.Host.Handlers;
using Fishing.Domain.Host.MessageBusConfiguration;
using Fishing.Domain.Infrastructure.Repository;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Infrastructure.Redis;
using Fishing.ReadModels.Infrastructure.Repositories;
using Fishing.ReadModels.Services;
using Fishing.ReadModels.Services.Projections;
using Fishing.ReadModels.Services.Repository;
using Fsihing.ReadModel.Host.Handlers;
using Messaging;
using Microsoft.Extensions.DependencyInjection;
using MessageBusConfiguration = Fsihing.ReadModel.Host.Configuration.MessageBusConfiguration;

namespace Fsihing.ReadModel.Host
{
    public static class DependencyResolver
    {
            
        public static void RegisterServices(IServiceCollection serviceCollection)
        {
            
//            _messagingConfiguration = new MessageBusConfiguration(
//                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=60",
//                "FishingJournal", "api");
            RegisterMessaging(serviceCollection);
        }

        private static void RegisterMessaging(IServiceCollection serviceCollection)
        {

            var amessagingConfiguration = new MessageBusConfiguration(
                "host=127.0.0.1;username=guest;password=guest;requestedHeartbeat=10;persistentMessages=false;timeout=3000",
                "FishingJournal", "api");
            serviceCollection.AddSingleton(amessagingConfiguration.Bus);
            serviceCollection.AddSingleton<IEventStore>(x => new EventStore.EventStore("ConnectTo=tcp://admin:changeit@localhost:1113; HeartBeatTimeout=500"));
            //            serviceCollection.AddSingleton<ICommandSender, CommandSender>();


            serviceCollection.AddScoped(typeof(ICacheProvider), typeof(RedisCacheProvider));
            serviceCollection.AddScoped(typeof(IViewRepository<FishingLog>), typeof(FishingLogProjectionRepository));
            serviceCollection.AddScoped(typeof(IProjection<FishingLog>), typeof(FishingLogProjection));
            //            serviceCollection.AddTransient<IEventHandler<Command>, FishingLogHandler>();
            serviceCollection.AddScoped(typeof(IEventHandler<FishingLogCreated>), typeof(FishingLogEventHandler));
            var serviceProvider = serviceCollection.BuildServiceProvider();
            amessagingConfiguration.Subscribe(serviceProvider);
        }
    }
}
