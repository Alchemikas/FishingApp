﻿using EasyNetQ;
using Fishing.Domain.Contracts.Events;
using Fsihing.ReadModel.Host.Handlers;
using Messaging;

namespace Fsihing.ReadModel.Host.Configuration
{
    public class MessageBusConfiguration : MessageBusConfigurationBase
    {
        public MessageBusConfiguration(string connectionString, string subscriptionId, string space) : base(connectionString, subscriptionId, space)
        {
        }

        public MessageBusConfiguration(IBus bus) : base(bus)
        {
        }

        protected override void SubscribeEvents()
        {
            RegisterHandler<FishingLogCreated, FishingLogEventHandler>();
        }
    }
}
