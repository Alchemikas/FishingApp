﻿using System;
using System.Threading.Tasks;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Services.Services;

namespace Fsihing.ReadModel.Host.Controller
{
    internal sealed class FishingLogController : BaseController
    {
        private readonly IFishingLogApplicationService _fishingLogApplicationService;

        public FishingLogController(IFishingLogApplicationService fishingLogApplicationService)
        {
            _fishingLogApplicationService = fishingLogApplicationService;
        }

        public async Task<FishingLog> GetFishinLog(Guid id)
        {
            return await _fishingLogApplicationService.GetFishingLog(id);
        }
    }
}
