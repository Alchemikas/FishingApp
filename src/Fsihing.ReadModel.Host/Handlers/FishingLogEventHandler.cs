﻿using System.Threading.Tasks;
using EventStore;
using Fishing.Domain.Contracts.Events;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Services;
using Fishing.ReadModels.Services.Repository;
using Messaging;

namespace Fsihing.ReadModel.Host.Handlers
{
    public class FishingLogEventHandler : ViewEvenHandlerBase<FishingLog>,
        IEventHandler<FishingLogCreated>,
        IEventHandler<FishingLogUpdated>
    {
        public FishingLogEventHandler(IEventStore eventStore,
            IProjection<FishingLog> projection,
            IViewRepository<FishingLog> repository)
            : base(eventStore, projection, repository)
        {
        }

        public async Task HandleAsync(FishingLogCreated @event)
        {
            await ProduceView($"FishingLogAggregte-{@event.Id}");
        }

        public async Task HandleAsync(FishingLogUpdated @event)
        {
            await ProduceView($"FishingLogAggregte-{@event.FishingLogId}");
        }

    }
}
