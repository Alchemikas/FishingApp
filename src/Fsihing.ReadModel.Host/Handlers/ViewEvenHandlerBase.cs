﻿using System.Threading.Tasks;
using EventStore;
using Fishing.ReadModels.Contracts.ReadModels;
using Fishing.ReadModels.Services;
using Fishing.ReadModels.Services.Repository;

namespace Fsihing.ReadModel.Host.Handlers
{
    public abstract class ViewEvenHandlerBase<TView> where TView : ViewEntity
    {
        private readonly IViewRepository<TView> _repository;
        private readonly IEventStore _eventStore;
        private readonly IProjection<TView> _projection;

        protected ViewEvenHandlerBase(IEventStore eventStore, IProjection<TView> projection, IViewRepository<TView> repository)
        {
            _eventStore = eventStore;
            _projection = projection;
            _repository = repository;
        }

        protected async Task ProduceView(string streamName)
        {
            EventStream stream = await _eventStore.LoadEventStreamAsync(streamName);
            TView entity = await _projection.ProjectAsync(new ProjectionData(stream));
            await _repository.SaveAsync(entity);
        }
    }
}
