﻿using System.Threading.Tasks;

namespace Messaging
{
    public interface IEventHandler<in TEvent>
    {
        Task HandleAsync(TEvent message);
    }
}
