﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Messaging
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public QueryDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TResult> Execute<TQuery, TResult>(TQuery query)
        {
            if (query == null)
            {
                throw new ArgumentException(nameof(query));
            }
            var handler = _serviceProvider.GetService<IQueryHandler<TQuery, TResult>>();

            if (handler == null)
            {
                throw new ArgumentException(nameof(query));
            }

            return await handler.Execute(query).ConfigureAwait(false);
        }
    }
}
