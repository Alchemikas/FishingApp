﻿using System.Threading.Tasks;

namespace Messaging
{
    public interface IQueryDispatcher
    {
        Task<TResult> Execute<TQuery, TResult>(TQuery query);
    }
}
