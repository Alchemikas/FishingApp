﻿using System.Threading.Tasks;
using EasyNetQ;
using Fishing.Domain.Contracts;
using Fishing.Domain.Contracts.Commands;

namespace Messaging
{
    public class CommandSender : ICommandSender
    {
        private readonly IBus _messageBus;

        public CommandSender(IBus messageBus)
        {
            _messageBus = messageBus;
        }

        public async Task PublishAsync(dynamic @event)
        {
            await _messageBus.PublishAsync(@event);
        }

        public async Task<TResponse> PublishAndGetAsync<TRequest, TResponse>(TRequest request) where TRequest : CommandBase where TResponse : MessageResponse
        {
            return await _messageBus.RequestAsync<TRequest, TResponse>(request);
        }
    }
}
