﻿using System;
using EasyNetQ;
using Microsoft.Extensions.DependencyInjection;
using IServiceProvider = System.IServiceProvider;

namespace Messaging
{
    public abstract class MessageBusConfigurationBase : IDisposable
    {
        private readonly string _subscriptionId;
        private readonly string _space;
        private readonly bool _disposeBus = false;

        public IBus Bus { get; }

        private IServiceProvider ServiceProvider { get; set; }

        protected MessageBusConfigurationBase(string connectionString, string subscriptionId, string space)
        {
            _subscriptionId = subscriptionId;
            _space = space;
            _disposeBus = true;
            Bus = RabbitHutch.CreateBus(connectionString);
        }

        protected MessageBusConfigurationBase(IBus bus)
        {
            Bus = bus;
        }

        public void Subscribe(IServiceProvider serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
            SubscribeEvents();
        }

        protected void RegisterHandler<TMessage, THandler>() where TMessage : class where THandler : IEventHandler<TMessage>
        {
            
            Bus.Subscribe<TMessage>(_subscriptionId, message =>
            {
                var handler = ServiceProvider.GetService<IEventHandler<TMessage>>();
                handler.HandleAsync(message);
            });
        }

        protected abstract void SubscribeEvents();
        
        public void Dispose()
        {
            if (_disposeBus)
                Bus.Dispose();
        }
    }
}
