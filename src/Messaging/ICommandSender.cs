﻿using System.Threading.Tasks;
using Fishing.Domain.Contracts;
using Fishing.Domain.Contracts.Commands;

namespace Messaging
{
    public interface ICommandSender
    {
        Task PublishAsync(dynamic @event);
        Task<TResponse> PublishAndGetAsync<TRequest, TResponse>(TRequest request) where TRequest : CommandBase
            where TResponse : MessageResponse;
    }
}
