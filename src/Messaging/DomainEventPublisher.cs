﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using EasyNetQ;
using Fishing.Domain.Core.Aggregates.Base;

namespace Messaging
{
    public class DomainEventPublisher : IDomainEventPublisher
    {
        private readonly IBus _messageBus;

        public DomainEventPublisher(IBus messageBus)
        {
            _messageBus = messageBus;
        }

        public void Publish(IAggregateRoot aggregateRoot)
        {
            var events = aggregateRoot.GetUncommittedEvents();

            foreach (var @event in events)
            {
                _messageBus.Publish(DynamicCast(@event, @event.GetType()));
            }
        }

        private dynamic DynamicCast(object entity, Type to)
        {
            MethodInfo openCast = typeof(DomainEventPublisher).GetMethod("Cast", BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy | BindingFlags.DeclaredOnly);
            MethodInfo closeCast = openCast.MakeGenericMethod(to);
            return closeCast.Invoke(entity, new[] { entity });
        }

        static T Cast<T>(object entity) where T : class
        {
            return entity as T;
        }
    }
}
