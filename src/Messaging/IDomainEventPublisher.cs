﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Core.Aggregates.Base;

namespace Messaging
{
    public interface IDomainEventPublisher
    {
        void Publish(IAggregateRoot aggregateRoot);
    }

//    public interface IAggregateRoot
//    {
//        int Version { get; }
//        void Apply(IEvent e);
//        void ClearUncommittedEvents();
//        IList<IEvent> GetUncommittedEvents();
//    }

    public interface IEvent 
    {
        Guid CorrelationId { get; }
        DateTime EventDate { get; }
    }
}
