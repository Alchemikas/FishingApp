﻿using System.Threading.Tasks;

namespace Messaging
{
    public interface IQueryHandler<in TQuery, TResult>
    {
        Task<TResult> Execute(TQuery query);
    }
}
