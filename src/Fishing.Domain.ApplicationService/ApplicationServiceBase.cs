﻿using System.Threading.Tasks;
using Fishing.Domain.Contracts;

namespace Fishing.Domain.ApplicationService
{
    public abstract class ApplicationServiceBase : IApplicationService, IApplicationServiceResponse
    {
        public void Handle(IMessage command)
        {
            ((dynamic)this).When((dynamic)command);
        }

        Task<MessageResponse> IApplicationServiceResponse.Handle(IMessage command)
        {
            return ((dynamic)this).When((dynamic)command);
        }
    }
}
