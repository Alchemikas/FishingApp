﻿using System;
using System.Threading.Tasks;
using EventStore;
using Fishing.Domain.Contracts;
using Fishing.Domain.Contracts.Commands;
using Fishing.Domain.Core.Aggregates;
using Fishing.Domain.Core.Repository;
using Messaging;


namespace Fishing.Domain.ApplicationService
{
    public class FishingLogApplicationService : ApplicationServiceBase
    {
        private readonly IRepository<FishingLogAggregte> _repository;
        private readonly IDomainEventPublisher _domainEventPublisher;

        public FishingLogApplicationService(IRepository<FishingLogAggregte> repository,
            IDomainEventPublisher domainEventPublisher)
        {
            _repository = repository;
            _domainEventPublisher = domainEventPublisher;
        }

        public async void When(UpdateFishingLog cmd)
        {
            var events = await _repository.GetById(cmd.FishingLogId).ConfigureAwait(false);
            var fishingLog = new FishingLogAggregte(events);
            fishingLog.Update(cmd);
            await _repository.SaveAsync(fishingLog);

            _domainEventPublisher.Publish(fishingLog);
        }

        public async void When(FishsCaught cmd)
        {
            var events = await _repository.GetById(cmd.FishingLogId).ConfigureAwait(false);
            var fishingLog = new FishingLogAggregte(events);
            fishingLog.FishsCaught(cmd);
            try
            {
                await _repository.SaveAsync(fishingLog);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async void When(CreateFishingLog cmd)
        {
            var fishingLog = new FishingLogAggregte();
            fishingLog.Create(cmd);

            await _repository.SaveAsync(fishingLog);

            _domainEventPublisher.Publish(fishingLog);
            
//            return await Task.FromResult(new MessageResponse() { Status = Status.Success, Uuid = fishingLog.State.Id });
        }

    }
}
