﻿using System.Threading.Tasks;
using Fishing.Domain.Contracts;

namespace Fishing.Domain.ApplicationService
{
    public interface IApplicationService
    {
        void Handle(IMessage command);
    }

    public interface IApplicationServiceResponse
    {
        Task<MessageResponse> Handle(IMessage command);
    }

}
