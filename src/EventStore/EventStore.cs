﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Fishing.Domain.Contracts.Events;
using Fishing.Domain.Core.Aggregates.Base;
using Newtonsoft.Json;

namespace EventStore
{
    public class EventStore : IEventStore
    {
        private const int WRITE_PAGE_SIZE = 500;
        private const int READ_PAGE_SIZE = 500;

        private static JsonSerializerSettings _serializerSettings;
        private readonly IEventStoreConnection _eventStoreConnection;

        public EventStore(string connectionString)
        {
            _eventStoreConnection = EventStoreConnection.Create(connectionString);
            _eventStoreConnection.ConnectAsync();
            _serializerSettings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None };
        }


        public async Task<EventStream> LoadEventStreamAsync(string streamName)
        {
            return await GetStreamById(streamName, 0, 500);
        }

        public Task<EventStream> LoadEventStreamAsync(string streamName, int skipEvents, int maxCount)
        {
            throw new NotImplementedException();
        }


        public async Task<EventStream> GetStreamById(string streamName, int skipEvents, int maxCount)
        {
            int sliceStart = skipEvents;
            int remainingCount = maxCount;

            EventStream stream = new EventStream();

            StreamEventsSlice currentSlice;
            do
            {
                int sliceCount = Math.Min(remainingCount, READ_PAGE_SIZE);

                currentSlice = await _eventStoreConnection.ReadStreamEventsForwardAsync(streamName, sliceStart, sliceCount, false);

                if (currentSlice.Status == SliceReadStatus.StreamNotFound)
                {
                    stream.Status = EventStreamStatus.StreamNotFound;
                    return stream;
                }

                if (currentSlice.Status == SliceReadStatus.StreamDeleted)
                    throw new ArgumentException("stream deleted");

                sliceStart = (int) currentSlice.NextEventNumber;
                stream.Version = (int) currentSlice.LastEventNumber;

                foreach (var evnt in currentSlice.Events)
                {
                    var asd = DeserializeEvent(evnt.OriginalEvent.Data, evnt.OriginalEvent.EventType);
                    stream.Events.Add(asd);
                }

            } while (remainingCount > 0 && !currentSlice.IsEndOfStream);


            return stream;
        }

        public async void SaveStream(IAggregateRoot aggregate, string streamName)
        {
            var newEvents = aggregate.GetUncommittedEvents();
            var originalVersion = aggregate.Version - newEvents.Count;

            var expectedVersion = originalVersion == 0 ? ExpectedVersion.NoStream : originalVersion;
            var eventsToSave = newEvents.Select(e => ToEventData(Guid.NewGuid(), e)).ToList();

            if (eventsToSave.Count < WRITE_PAGE_SIZE)
            {
                await _eventStoreConnection.AppendToStreamAsync(streamName, expectedVersion, eventsToSave).ConfigureAwait(false);
            }
            else
            {
                var transaction = await _eventStoreConnection.StartTransactionAsync(streamName, expectedVersion).ConfigureAwait(false);

                var position = 0;
                while (position < eventsToSave.Count)
                {
                    var pageEvents = eventsToSave.Skip(position).Take(WRITE_PAGE_SIZE);
                    await transaction.WriteAsync(pageEvents).ConfigureAwait(false);
                    position += WRITE_PAGE_SIZE;
                }

                await transaction.CommitAsync();
            }

            aggregate.ClearUncommittedEvents();
        }

        private static IEvent DeserializeEvent(byte[] data, string eventType)
        {
            return (IEvent)JsonConvert.DeserializeObject(Encoding.UTF8.GetString(data), Type.GetType(eventType));
        }


        private EventData ToEventData(Guid eventId, object @event)
        {
            IEvent e = @event as IEvent;
            if (e != null)
            {
                e.Header.Id = eventId;
            }

            byte[] metaData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(@event, _serializerSettings));

            string typeName = @event.GetType().AssemblyQualifiedName;

            return new EventData(eventId, typeName, true, metaData, null);
        }


        public async Task UpdateStreamAsync(string streamName, Guid correlationId, long expectedVersion, object @event)
        {
            await _eventStoreConnection.DeleteStreamAsync(streamName, ExpectedVersion.Any);

            if (expectedVersion != ExpectedVersion.Any)
            {
                long originalVersion = expectedVersion < 0 ? ExpectedVersion.NoStream : expectedVersion - 1;
                expectedVersion = originalVersion == -1 ? ExpectedVersion.NoStream : originalVersion;
            }

            EventData eventToSave = ToEventData(correlationId, @event);

            await _eventStoreConnection.AppendToStreamAsync(streamName, ExpectedVersion.NoStream, eventToSave)
                .ConfigureAwait(false);
        }

        public async Task UpdateStreamAsync(string streamName, Guid correlationId, long expectedVersion,
            ICollection<IEvent> events)
        {
            if (expectedVersion != ExpectedVersion.Any)
            {
                long originalVersion = expectedVersion < 0 ? ExpectedVersion.NoStream : expectedVersion - events.Count;
                expectedVersion = originalVersion == -1 ? ExpectedVersion.NoStream : originalVersion;
            }

            var eventsToSave = events.Select(e => ToEventData(Guid.NewGuid(), e)).ToList();

            if (eventsToSave.Count < WRITE_PAGE_SIZE)
            {
                await
                    _eventStoreConnection.AppendToStreamAsync(streamName, expectedVersion, eventsToSave)
                        .ConfigureAwait(false);
            }
            else
            {
                var transaction =
                    await _eventStoreConnection.StartTransactionAsync(streamName, expectedVersion).ConfigureAwait(false);

                var position = 0;
                while (position < eventsToSave.Count)
                {
                    var pageEvents = eventsToSave.Skip(position).Take(WRITE_PAGE_SIZE);
                    await transaction.WriteAsync(pageEvents).ConfigureAwait(false);
                    position += WRITE_PAGE_SIZE;
                }

                await transaction.CommitAsync();
            }
        }

    }
}
