﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fishing.Domain.Contracts.Events;

namespace EventStore
{
    public interface IEventStore
    {
        Task<EventStream> LoadEventStreamAsync(string streamName);
        Task<EventStream> LoadEventStreamAsync(string streamName, int skipEvents, int maxCount);
        Task UpdateStreamAsync(string streamName, Guid correlationId, long expectedVersion, ICollection<IEvent> events);
        Task UpdateStreamAsync(string streamName, Guid correlationId, long expectedVersion, object @event);
    }
}
