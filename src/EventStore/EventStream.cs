﻿using System;
using System.Collections.Generic;
using System.Text;
using Fishing.Domain.Contracts.Events;

namespace EventStore
{
    public class EventStream
    {
        public int Version;
        // all events in the stream
        public List<IEvent> Events = new List<IEvent>();
        // A status of stream load:
        public EventStreamStatus Status = EventStreamStatus.Success;
    }

    public enum EventStreamStatus
    {
        Success,
        StreamNotFound,
    }
}
